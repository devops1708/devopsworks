## Demo Deployment Steps

### **Containerisation**
***

>**Step1:** Clone the GO App Github repo

>**Step2:** Create a  ` Dockerfile ` in the location ` deployment/build/ `

>**Step3:** Copy and Paste this below content in Dockerfile

       
``` 
FROM golang:latest
ENV GO111MODULE=on
WORKDIR /app
COPY go.mod go.sum Makefile ./
COPY cmd/pinger/*.go ./
RUN go build -o /main
RUN apt-get update && apt-get install -y iputils-ping
Volume /app/data
EXPOSE 8000
CMD [ "/main" ] 

```

#### **Dockerfile Explanation**
***


**golang:latest**   ==> Base image which will be used to pull the golang image from public dockerhub. If we will use golang base image, it s not required to install any other components to run the go application

**WORKDIR /app**  ==> This will set the path in the container

**COPY** ==> This will be used to copy the files like go.sum, go.mod,Makefile and all .go lang files into the workdir(/app) location in the container. This will be used to build the go application in the building time

**go build**  ==> This will build the go application along with download the respective dependcies in go application with output option

**Volume /app/data**  ==> It will be used to create the volume mapping with the container location. Restore the data using volume incase of any disaters to the containers

**apt-get install -y iputils-ping**   ==> This is used to check the ping status from one container to another container. Mostly by defualt ping will not be avilable in all the images. so escpecially installing ping and it will be used in compose containers to check the ping status.
> some times timeout due to connecting debain server to update and install packages. just retry the build if in case failure due to time out with debain server

**EXPOSE:8000**  ==> Expose the port to run the applicaiton with this port

**CMD["/main"]**  ==> This will be used to run the application  


>**Step4:** Create the Docker build using this below command


``` Docker build 

docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .


-t ==> tag name for the docker image
-f  ==> Dockerfile location
```
>**Step5:** RUN the Docker image using this below command

``` Docker RUN 

docker run -i -d -p 8000:8000 devops/pinger:latest
docker ps

-p ==>  Port 8000
devops/pinger:latest  ==> docker image used to run the container
-d  => run in the background mode
docker ps ==> to list the running containers
```

>**Step6:** Verify the application using host url with port 8000

``` Access Application
ex: http://<hosname>:8000
Verify the response of the url : hello world

Check the health endpoint

http://localhost:8000/healthz
Verify the response of the url : alive

```
***

### **Pipeline**
***

>**Step1:** Clone the GO App Github repo

>**Step2:** Create a  ` .gitlab-ci.yml ` in the root location of project

>**Step3:** Copy and Paste this below content in `.gitlab-ci.yml`

       
``` 
image: docker:latest

variables:
  DOCKER_DRIVER: overlay

services:
  - docker:dind

stages:
  - build
  - run
  - viewPipeline
  - compose
  - viewReadMe
 
  
before_script:
  - "apk update && apk add make"

binary:
  stage: build
  image: golang:latest
  script:
    - make dep
    - make build
  artifacts:
    paths:
      - ./

#build docker image
build:
  stage: build
  needs: ["binary"]
  script:
    #- docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .
    - make docker_image
    - docker images
    - mkdir build
    - make docker_tar
  artifacts:
      paths:
        - ./build
      expire_in: 1 week


#load and run DockerImage
Extract_TarImage_and_RUN:
  stage: run
  needs: ["build"]
  script:
    - make docker_untar
    - docker images
    - make docker_testrun
    - docker ps


#view gitlabci file
View_CI_Pipeline:
  stage: viewPipeline
  needs: []
  script:
    - make verify_pipeline

#compose multi containers
Compose_Multi_Containers:
  stage: compose
  needs: []
  before_script:
    - apk --no-cache add docker-compose
    - docker-compose --version
  script:
    - make testenv
    
#view deployment steps
View_Deployment_Steps:
  stage: viewReadMe
  needs: []
  script:
    - make verify_readme



```

#### **CI stages Explanation**
***

**image**   ==> This will be the `docker:latest` image which will be used as default image to perform all stage operations 

**before_script**   ==> It will download any dependcies for the application before starting the stage operations

>**stages**   ==> List all the stages in sequence execution 

**build stage**   ==> this will perform the build the application, dockrize the application build in terms of images and save the images in `.tar` format

**build artifcats** => the compiled build and docker .tar images will be saved in the respective job artifacts of the pipeline and also downlod it from pipeline.

 > 2 artifcats are used with respect to stages for binary and docker builds. 1. store binary build of go application 2. store docker build image .tar format
**run stage**  => it will be used to create the docker container with devops/pinger:latest image with expose of port 8000 in background mode. After that verify the application endpoints of the app

**viewPipeline stage**  => It will list the content of .gitlab-ci.yml file

**compose stage**  => Docker compose will be used to run multiple containers. In this two pinger services are created and running on ports(8001 and 8002) in the same network zone. with the help of network, both services can ping each other 

**viewReadme stage**  => It will list the contents of ReadME.md file


>**Step4:** Trigger manually or automated based on push changes in master branch

>**Step5:** verify all the stgages output as per the requirment



***

### **Environment**
***

>**Step1:** Clone the GO App Github repo

>**Step2:** Create a  `docker-compose.yml ` in the location `\deployments`

>**Step3:** Copy and Paste this below content in `docker-compose.yml`

       
``` 
version: "3.6"

services:
  service1:
    build:
      context: ..
      dockerfile: deployments/build/Dockerfile
    container_name: service1
    volumes:
       - service1-data:/app/service1-data
    ports:
      - "8001:8000"
    networks:
      - godemo
  
  service2:
    build:
      context: ..
      dockerfile: deployments/build/Dockerfile
    container_name: service2
    volumes:
       - service2-data:/app/service1-data
    ports:
      - "8002:8000"
    networks:
      - godemo
    

volumes:
  service1-data:
  service2-data:
 
networks:
  godemo:

```

#### **Compose Explanation**
***

**Pinger service1**   ==> It will build the service1 container using the dockerfile which will be located in deployment/build. Also its have exposed on port 8001 and created volume for any failures of container data loss. The container will be attached to network "godemo".

**Pinger service2**   ==> It will build the service2 container using the dockerfile which will be located in deployment/build. Also its have exposed on port 8002 and created volume for any failures of container data loss. The container will be attached to network "godemo".

**network**  ==> common network will be used to interact both containers

>**Step4:** Create the Docker compose containers using this below command


``` 

docker-compose -f ./deployments/docker-compose.yml up -d -V

docker-compose -f ./deployments/docker-compose.yml ps
View the running services containers

```

>**Step5:** checking ping statuc to interact both service containers using this below command


``` 
    - docker exec service1 ping -w5 service2
    - docker exec service2 ping -w5 service1

```
**using service name, it interact each other with ping check for 5 seconds**

***

### **Documentation**
***

>**ReadMe** it will shows the in-detail deployment steps for the containerzation,pipeline and compose. The file will be located in the location `docs\README.md`


